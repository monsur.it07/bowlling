#Information this project has been done with **Ruby 2.5.2** and **Rails 4.2.8** version


# SETUP
* Clone the project
* Change the database.yml file accordingly
### Run application
* bundle install
* rake db:create
* rake db:migrate
* rails s

#### To run the test suit
*  bundle exec rspec test/models/game_test.rb
