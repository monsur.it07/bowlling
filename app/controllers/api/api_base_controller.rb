class Api::ApiBaseController < ApplicationController
  # rescue_from ActiveRecord::RecordNotFound, with: :_404
  # rescue_from ActiveRecord::InvalidForeignKey, with: :_404
  # rescue_from ActiveRecord::RecordInvalid, with: :_400
  # rescue_from ActiveRecord::StatementInvalid, with: :_400
  # rescue_from ActiveRecord::RecordNotUnique, with: :_406
  # rescue_from ActiveModel::ForbiddenAttributesError, with: :_400
  # rescue_from ActionController::BadRequest, with: :_400
  # rescue_from NoMethodError, with: :_501
  # rescue_from NameError, with: :_400
  # rescue_from ParseError, with: :parse_error
  # rescue_from ArgumentError, with: :_501
  # rescue_from RangeError, with: :_400
  # rescue_from ActionController::RoutingError, with: :routing_error
  # rescue_from PG::InvalidTextRepresentation, with: :_400
  # rescue_from PG::ForeignKeyViolation, with: :_400

  skip_before_action :verify_authenticity_token


  def routing_error
    render json: "Routing Error", status: 501
  end

  protected

  def not_authorised(e)

    render status: 401, json: {error: e.message }.as_json

  end

  def parse_error(e)

    render status: 409, json: {error: e.message }.as_json
  end

  def login_error(e)

    render status: 401, json: {error: e.message }.as_json
  end

  def _406(exception)
    render status: :not_acceptable, json: { error: exception.message }.as_json
  end
  def _400(e)

    render status: 400, json: { error: e.message }.to_json
  end
  def _404(e)

    render status: 404, json:  { error: e.message }.as_json
  end
  def _501(exception)

    render status:  501, json: { error: exception.message }.as_json
  end

end
