class Api::V1::GamesController < Api::ApiBaseController

  before_action :fetch_game, only: [:rolls, :destroy, :score_by_game, :score_by_frame]

  def index
    @games = Game.order(:created_at).last(10)
    render json: @games.as_json
  end

  def show
    render json: @game.as_json
  end

  # api url /api/v1/games, method: post
  def create
    @game = Game.new(params[:game])
    if @game.save
      render json: @game.as_json
    else
      render json: {message: "Game can't be created"}
    end
  end

  # api url /api/v1/games/1/rolls, method: patch
  # count the rolls and update the frame
  def rolls
    begin
      @game.roll!(params[:pins_down].to_i)
    rescue
      flash[:notice] = "Invalid Play!."
    end
    render json: @game.as_json
  end

  # api url /api/v1/games/1/game_score, method: get
  # get the game score
  def score_by_game
    render json: {score: @game.score}
  end

  # api url /api/v1/games/1/frame_score, method: get
  # get the score by frame
  def score_by_frame
    frames = @game.frames.where(number: params[:frame_number]).order(:number)
    score = frames.map(&:score).inject(:+)
    render json: {score: score}
  end

  # api url /api/v1/games/1, method: delete
  def destroy
    @game.destroy
    render json: {message: "Game deleted successfully."}
  end

  private
  def fetch_game
    @game = Game.find(params[:id])
  end
end
