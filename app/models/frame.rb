class Frame < ActiveRecord::Base
  belongs_to :game


  # update the pins of the respective rolls for a frame
  def roll!(number_of_pins)

    if self.finished? || number_of_pins > 10 || number_of_pins < 0
      raise RuntimeError, "InvalidPlay"

    elsif ! self.pins_down_by_roll_one
      self.pins_down_by_roll_one = number_of_pins

    elsif self.last? and self.pins_down_by_roll_two
      # if the current frame is complete then update the next frame
      frame = self.next_frame
      frame.pins_down_by_roll_one = number_of_pins
      frame.save

    elsif (self.pins_down_by_roll_one + number_of_pins < 11) || (self.last? && self.strike?)
      self.pins_down_by_roll_two = number_of_pins

    else
      raise RuntimeError, "InvalidPlay"
    end

    self.save
  end

  # score calculation frame wise
  def score
    total = (self.pins_down_by_roll_one || 0) + (self.pins_down_by_roll_two || 0)
    if self.last?
      total + (self.next_frame.pins_down_by_roll_one || 0)
    elsif self.strike?
      total + strike_bonus
    elsif self.spare?
      total + spare_bonus
    else
      total
    end
  end

  def finished?
    if self.last?

      return(self.pins_down_by_roll_two && ((!self.spare_or_strike?) || self.next_frame.pins_down_by_roll_one))
    else
      return(self.strike? || self.pins_down_by_roll_two)
    end
  end

  # if the roll is a strike
  def strike?
    self.pins_down_by_roll_one == 10
  end

  # if the roll is a spare or strike
  def spare_or_strike?
    (self.pins_down_by_roll_one || 0) + (self.pins_down_by_roll_two || 0) > 9
  end

  # if the roll is a spare
  def spare?
    !strike? && spare_or_strike?
  end

  # bonus for spare
  def spare_bonus
    (self.next_frame.pins_down_by_roll_one || 0 )
  end

  # bonus for strike
  def strike_bonus
    bonus = self.spare_bonus
    if bonus == 10
      bonus += (next_frame(self.number + 2).pins_down_by_roll_one ||0)
    else
      bonus += (next_frame.pins_down_by_roll_two || 0)
    end
    bonus
  end

  # Find the last frame of the game
  def last?
    self.number == 10
  end

  # get the next frame
  def next_frame(frame_number=nil)

    self.game.find_frame_by_number( frame_number||self.number + 1)
  end

end
