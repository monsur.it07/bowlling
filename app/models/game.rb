class Game < ActiveRecord::Base
  has_many :frames

  after_initialize :setup_game

  # update the frame with the number of pins that are knock down
  def roll!(number_of_pins)
    raise "InvalidPlay" if finished?
    frame = self.find_current_frame
    frame.roll! number_of_pins
    self.current_frame_number += 1 if frame.finished?
    self.save
  end


  # if the game is finished
  def finished?
    self.current_frame_number == 11
  end

  # calculate the game score
  def score
    self.game_frames.map(&:score).inject(:+)
  end

  # fetch the current frame
  def find_current_frame
    self.frames.order(:number)[self.current_frame_number.to_i - 1]
  end

  # fetch all frames of the game
  def game_frames
    self.frames.order(:number)[0..9]
  end

  # fetch a specific frame of a game
  def find_frame_by_number(number)
    self.frames.order(:number)[number - 1]
  end

  # create the game with all the frames
  def setup_game
    return if self.frames.present?
    self.current_frame_number = 1
    11.times do |frame_number|
      self.frames.build { |frame| frame.number = frame_number + 1 }
    end
    self.save
  end
end
