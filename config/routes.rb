Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :games do
        member do
          patch 'rolls', to: 'games#rolls'
          get 'game_score', to: 'games#score_by_game'
          get 'frame_score/:frame_number', to: 'games#score_by_frame'
        end
      end
    end
  end
end
