class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.boolean :is_completed, default: false
      t.integer :current_frame_number

      t.timestamps null: false
    end
  end
end
