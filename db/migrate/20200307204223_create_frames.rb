class CreateFrames < ActiveRecord::Migration
  def change
    create_table :frames do |t|
      t.integer :game_id
      t.integer :number
      t.integer :pins_down_by_roll_one
      t.integer :pins_down_by_roll_two
      t.integer :pins_down_by_roll_three
      t.integer :points
      t.timestamps null: false
    end
  end
end
