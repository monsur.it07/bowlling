require './test/test_helper'
class GameTest < ActiveSupport::TestCase
  describe Game do
    before(:each) do
      @game = Game.new
    end

    it "should have 10 frames + 1 extra (used for the third ball in last frame)" do
      # @game.frames.count.to_s.should == '11'
      expect(@game.frames.length.to_i).to eq(11)
    end

    it "should have numbered frames" do
      @game.frames.map(&:number).should == [1,2,3,4,5,6,7,8,9,10,11]
    end

    it "should calculate a zero points game" do
      repeat_roll!(20, 0)
      @game.score.should == 0
    end

    it "should calculate an all ones game" do
      repeat_roll!(20, 1)
      @game.score.should == 20
    end

    it "should calculate a spare" do
      roll_spare!
      @game.roll!(3)
      @game.score.should == 16
    end

    it "should test consecutive spares each get a one roll bonus" do
      roll_spare!
      roll_spare!
      repeat_roll!(1,5)
      repeat_roll!(15,0)
      @game.score.should == 35

    end

    it "should calculate a strike" do
      roll_strike!
      @game.roll!(3)
      @game.roll!(4)
      @game.score.should == 24
    end

    it "should test two bonus rolls after a strike in the last frame can score more than 10 points if one is a strike" do
      repeat_roll!(18,0)
      roll_strike!
      roll_strike!
      repeat_roll!(1,5)
      @game.score.should == 25

    end

    it "should calculate a perfect game" do
      12.times { roll_strike! }
      @game.score.should == 300
    end

    it "should test consecutive strikes each get the two roll bonus" do
      3.times { roll_strike! }
      repeat_roll!(1,6)
      repeat_roll!(1,3)
      repeat_roll!(12,0)

      @game.score.should == 84
    end

    it "should calculate the score during the game" do
      @game.roll!(10)
      @game.roll!(10)
      @game.roll!(3)
      @game.roll!(7)
      @game.roll!(10)
      @game.score.should == 73
    end

    it "should test strikes with the two roll bonus do not get bonus rolls" do
      repeat_roll!(18,0)
      roll_strike!
      roll_strike!
      roll_strike!
      @game.score.should == 30
    end


    it "should calcualte correct score when there is spare bonus in last frame" do
      repeat_roll!(18,2)
      roll_spare!
      @game.roll!(3)
      @game.score.should == 49
    end

    it "should finish after rolling 20 balls" do
      repeat_roll!(20,2)
      @game.finished?.should == true
    end

    it "should test score a game with no strikes or spares" do
      repeat_roll!(20,4)
      @game.score.should == 80
    end

    it "should not be able calculate score with negative pins" do
      expect { repeat_roll!(20,-1) }.to raise_error( RuntimeError, "InvalidPlay")
    end

    it "should not be able calculate score more than 10 pins" do
      expect { repeat_roll!(20,6) }.to raise_error( RuntimeError, "InvalidPlay")
    end


    ####################
    ## Helper methods ##
    ####################

    def repeat_roll!(number_of_rolls, pins)
      (1..number_of_rolls).each { @game.roll!(pins) }
    end

    def roll_spare!
      @game.roll!(5)
      @game.roll!(5)
    end

    def roll_strike!
      @game.roll!(10)
    end

  end

end
